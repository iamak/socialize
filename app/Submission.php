<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
	protected $table = 'submissions';

	protected $perPage = 6;

    protected $fillable = ['name','email','phoneno','country','age','gender','dish_name','dish_description','dish_photo','verified','published'];

    /**
     * Published Items
     */

    public function scopePublished($query)
    {
    	return $query->where('published', 1);
    }

    /**
     * Validation Rules
     * @return array
     */
    
    protected function validationRules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email',
            'phoneno' => 'required|integer',
            'country' => 'required',
            'age' => 'required|integer',
            'gender' => 'required',
            'dish_name' => 'required',
            'dish_description' => 'required',
            'dish_photo' => 'required|image',
        ];
    }

    /**
     * Validation Messages
     * @return array messages string in array
     */
    protected function validationMessages()
    {
        return [];
    }
    
}
