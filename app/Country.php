<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['name','official_name','native_names','toplevel_domain','cca2','ccn3','cca3','cioc','currency','calling_code','capital','alt_spellings','languages','translations','region','subregion','latlng','demonym','borders','area','active','verified','published','blocked','deleted','created_by','created_ip','updated_by','updated_ip'];

     /**
     * Published Items
     */

    public function scopePublished($query)
    {
        return $query->where('published', 1);
    }

    public function setNativeNamesAttribute($value)
    {
        $this->attributes['native_names'] = json_encode($value);
    }

    public function setToplevelDomainAttribute($value)
    {
        $this->attributes['toplevel_domain'] = json_encode($value);
    }

    public function setCurrencyAttribute($value)
    {
        $this->attributes['currency'] = json_encode($value);
    }

    public function setCallingCodeAttribute($value)
    {
        $this->attributes['calling_code'] = json_encode($value);
    }

    public function setAltSpellingsAttribute($value)
    {
        $this->attributes['alt_spellings'] = json_encode($value);
    }

    public function setLanguagesAttribute($value)
    {
        $this->attributes['languages'] = json_encode($value);
    }

    public function setTranslationsAttribute($value)
    {
        $this->attributes['translations'] = json_encode($value);
    }

    public function setLatlngAttribute($value)
    {
        $this->attributes['latlng'] = json_encode($value);
    }

    public function setBordersAttribute($value)
    {
        $this->attributes['borders'] = json_encode($value);
    }
    
}
