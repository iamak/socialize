<?php

namespace App\Http\Controllers;

use App\Submission;

use App\Country;

use Validator;

use Illuminate\Http\Request;

class SocialAppController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Countries = Country::published()->pluck('name','name')->toArray();
        $data['Countries'] = $Countries;
        return view('home.register', $data);
        
    }

    public function gallery()
    {
        $data['Submissions'] = Submission::published()->latest()->paginate();
        return view('home.gallery', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validationRules = Submission::validationRules();
        $validationMessages = Submission::validationMessages();

        $validator = Validator::make($input, $validationRules, $validationMessages);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $input['published'] = 1;

        $path = $request->dish_photo->store('uploads/images');

        $input['dish_photo'] = $path;

        $Submission = Submission::create($input);

        return redirect('/gallery');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Submission $submission)
    {
        $data['Submission'] = $submission;
        if ($request->ajax()) {
            return view('home.details', $data);
        }
        return redirect()->route('login');
    }
}
