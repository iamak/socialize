/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmory imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmory exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		Object.defineProperty(exports, name, {
/******/ 			configurable: false,
/******/ 			enumerable: true,
/******/ 			get: getter
/******/ 		});
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports) {

eval("/**\r\n * Social App object\r\n * @type {object}\r\n */\r\nwindow.socialApp = window.socialApp || {};\r\n/**\r\n * Show POpup\r\n * @param  {css selector} popupName\r\n * @return {void}\r\n */\r\nwindow.socialApp.showPopup = function(popupName) {\r\n    this.closePopup();\r\n    $(popupName).addClass('active');\r\n};\r\n\r\nwindow.socialApp.closePopup = function() {\r\n    $('.popup-holder').removeClass('active');\r\n    $('.gallery-details').removeClass('active');\r\n};\r\n\r\n$(document).ready(function() {\r\n    $('[data-popup-link]').on('click', function(e) {\r\n        if ($(this).attr('data-popup-link') != '') {\r\n            window.socialApp.showPopup($(this).attr('data-popup-link'));\r\n        }\r\n    });\r\n    $('[data-popup-close]').on('click', function(e) {\r\n        window.socialApp.closePopup();\r\n    });\r\n});\r\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiMC5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9yZXNvdXJjZXMvYXNzZXRzL2pzL2FwcC5qcz84YjY3Il0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxyXG4gKiBTb2NpYWwgQXBwIG9iamVjdFxyXG4gKiBAdHlwZSB7b2JqZWN0fVxyXG4gKi9cclxud2luZG93LnNvY2lhbEFwcCA9IHdpbmRvdy5zb2NpYWxBcHAgfHwge307XHJcbi8qKlxyXG4gKiBTaG93IFBPcHVwXHJcbiAqIEBwYXJhbSAge2NzcyBzZWxlY3Rvcn0gcG9wdXBOYW1lXHJcbiAqIEByZXR1cm4ge3ZvaWR9XHJcbiAqL1xyXG53aW5kb3cuc29jaWFsQXBwLnNob3dQb3B1cCA9IGZ1bmN0aW9uKHBvcHVwTmFtZSkge1xyXG4gICAgdGhpcy5jbG9zZVBvcHVwKCk7XHJcbiAgICAkKHBvcHVwTmFtZSkuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xyXG59O1xyXG5cclxud2luZG93LnNvY2lhbEFwcC5jbG9zZVBvcHVwID0gZnVuY3Rpb24oKSB7XHJcbiAgICAkKCcucG9wdXAtaG9sZGVyJykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG4gICAgJCgnLmdhbGxlcnktZGV0YWlscycpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcclxufTtcclxuXHJcbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCkge1xyXG4gICAgJCgnW2RhdGEtcG9wdXAtbGlua10nKS5vbignY2xpY2snLCBmdW5jdGlvbihlKSB7XHJcbiAgICAgICAgaWYgKCQodGhpcykuYXR0cignZGF0YS1wb3B1cC1saW5rJykgIT0gJycpIHtcclxuICAgICAgICAgICAgd2luZG93LnNvY2lhbEFwcC5zaG93UG9wdXAoJCh0aGlzKS5hdHRyKCdkYXRhLXBvcHVwLWxpbmsnKSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSk7XHJcbiAgICAkKCdbZGF0YS1wb3B1cC1jbG9zZV0nKS5vbignY2xpY2snLCBmdW5jdGlvbihlKSB7XHJcbiAgICAgICAgd2luZG93LnNvY2lhbEFwcC5jbG9zZVBvcHVwKCk7XHJcbiAgICB9KTtcclxufSk7XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyByZXNvdXJjZXMvYXNzZXRzL2pzL2FwcC5qcyJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7QUFJQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTsiLCJzb3VyY2VSb290IjoiIn0=");

/***/ }
/******/ ]);