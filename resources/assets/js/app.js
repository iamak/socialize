/**
 * Social App object
 * @type {object}
 */
window.socialApp = window.socialApp || {};
/**
 * Show POpup
 * @param  {css selector} popupName
 * @return {void}
 */
window.socialApp.showPopup = function(popupName) {
    this.closePopup();
    $(popupName).addClass('active');
};

window.socialApp.closePopup = function() {
    $('.popup-holder').removeClass('active');
    $('.gallery-details').removeClass('active');
};

$(document).ready(function() {
    $('[data-popup-link]').on('click', function(e) {
        if ($(this).attr('data-popup-link') != '') {
            window.socialApp.showPopup($(this).attr('data-popup-link'));
        }
    });
    $('[data-popup-close]').on('click', function(e) {
        window.socialApp.closePopup();
    });
});
