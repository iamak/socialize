<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Socialize App</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">    
    {{ Html::style('css/app.css') }}
    {{-- Page specific styles --}}
    @yield('pagestyles')
</head>
<body>
    <div class="page-wrapper">
        <div class="popup-holder terms-popup">
            <a href="#" class="close" data-popup-close>X</a>
            <div class="inner-contents">
                <h3>Terms and Conditions</h3>
            </div>
            <!-- /.inner-contents -->
        </div>
        <!-- /.popup-holder.terms-popup -->
        <div class="popup-holder privacy-popup">
            <a href="#" class="close" data-popup-close>X</a>
            <div class="inner-contents">
                <h3>Privacy Policy</h3>
            </div>
            <!-- /.inner-contents -->
        </div>
        <!-- /.popup-holder.terms-popup -->

        @yield('content')
        <footer class="footer clear">
            <a href="#" data-popup-link=".privacy-popup" class="footer-links privacy-policy">Privacy Policy</a>
            <a href="#" data-popup-link=".terms-popup" class="footer-links terms-conditions">Terms & Conditions</a>
        </footer>
        <!-- /.footer -->
    </div>
    <!-- /.page-wrapper -->    
    {{ Html::script('vendor/bower_components/jquery/dist/jquery.min.js') }}
    {{ Html::script('js/app.js') }}
    {{-- Page specific scripts --}}
    @yield('pagescripts')
</body>
</html>