<div class="details-holder">
    <a href="#" class="close" onclick="window.socialApp.closePopup();">Close X</a>
    <a class="image-preview">
        {{ Html::image($Submission->dish_photo, $Submission->dish_name) }}
    </a>
    <div class="details-panel clear">
        <div class="col col-3">
            <div class="block name">{{ $Submission->name }}</div>
            <div class="block name">Entry no. {{ $Submission->id }}</div>
        </div>
        <div class="col col-5">
            <div class="block">{{ $Submission->dish_name }}</div>
            <div class="block">
                {{ $Submission->dish_description }}
            </div>
        </div>
        <div class="col col-4">
            {{ Form::button('',['type' => 'button', 'class'=>'btn-like-2']) }} {{ Form::button('Share',['type' => 'button', 'class'=>'btn-share']) }}
        </div>
    </div>
    <!-- /.details-panel -->
</div>
<!-- /.details-holder -->
