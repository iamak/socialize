@extends('layout')
<!--  -->
@section('content')
<!-- Registration content starts -->
<div class="register-section gallery clear">
    <a href="/" class="logo-holder">        
        {{ Html::image('img/glad-logo.png','Glad Arabia Logo') }}
    </a>
    <div class="glad-chef">
        {{ Html::image('img/glad-chef.png','Glad Chef') }}
    </div>
    <div class="gallery-details">
    </div>
    <!-- /.gallery-details -->
    <div class="register-form-container gallery-holder">
        <div class="register-header">
            <h2 class="italic">So you think you can cook?</h2>
            <a href="/" class="submit-now">Submit your dish!</a>
            <h2 class="gallery-title">Competition Gallery</h2>
            <h3 class="gallery-mini-title">Vote for your favourite dish or submit your own</h3>
        </div>
        <!-- /.register-header -->
        <div class="form-holder">
            <div class="search-bar-holder clear">
                {!! Form::open(['route' => 'register.post', 'id'=>'searchEntries']) !!}
                <div class="input-group horizontal">
                    {{ Form::label('keyword', 'Search:') }} {{ Form::text('keyword','',['maxlength' => 20, 'placeholder' => 'Name/Contestant no.']) }}
                </div>
                <!-- /.input-group -->
                {!! Form::close() !!}
            </div>
            <!-- /.search-bar-holder -->
            <div class="gallery-container clear">
                <ul class="gallery-items-list clear">
                    @foreach($Submissions as $Submission)
                    <li class="item">
                        <div class="likes-holder">
                            43 Likes {{ Form::button('',['type' => 'button', 'class'=>'btn-like']) }}
                        </div>
                        <!-- /.likes-holder -->
                        <div class="thumb-holder">
                            <a class="ajaxlink" href="/gallery/details/{{ $Submission->id }}">
                                <span class="info">
                                        <span class="block title">{{ $Submission->dish_name }}</span>
                                <span class="block name">{{ $Submission->name  }}</span>
                                <span class="block id">Entry no. {{ $Submission->id }} </span>
                                </span>
                                {{ Html::image($Submission->dish_photo, $Submission->dish_name) }}
                            </a>
                        </div>
                        <!-- /.thumb-holder -->
                    </li>
                    @endforeach
                    <!-- /.li.item -->
                </ul>
                <!-- /ul.gallery-items-list -->
            </div>
            <!-- /.gallery-container -->
            {{ $Submissions->links() }}
        </div>
        <!-- /.form-holder -->
    </div>
    <!-- /.register-form-container -->
</div>
<!-- /.register-section -->
@endsection @section('pagescripts') {{ Html::script('vendor/bower_components/masonry/dist/masonry.pkgd.min.js') }}
<script>
$(window).on('load', function() {
    $('.gallery-items-list').masonry({
        itemSelector: '.item'
    });
});
$(document).ready(function() {
    $('.ajaxlink').on('click', function(e) {
        e.preventDefault();
        $.get($(this).attr('href'), function(data) {
            $('.gallery-details').html(data).addClass('active');
        });
        return false;
    })
});
</script>
@endsection
