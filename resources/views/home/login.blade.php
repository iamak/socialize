@extends('layout')
<!--  -->
@section('content')
<!-- Registration content starts -->
<div class="login-bg clear">
    <a href="#" class="logo-holder-big">
        <span class="title">So you think you can cook?</span> {{ Html::image('img/login-glad-logo.png','Glad Arabia Logo') }}
    </a>
    <div class="login-form clear">
        {!! Form::open(['route' => 'register.post', 'id'=>'searchEntries']) !!}
        <div class="input-group horizontal clear">
            {{ Form::label('email', 'Username:') }} 
            {{ Form::text('email','',['maxlength' => 50, 'required' => 'required']) }}
        </div>
        <!-- /.input-group -->
        <div class="input-group horizontal clear">
            {{ Form::label('password', 'Password:') }} 
            {{ Form::password('password',['required' => 'required']) }}
        </div>
        <!-- /.input-group -->
        {{ Form::button('Enter',['type' => 'button', 'class'=>'btn-share btn-enter']) }}
        {!! Form::close() !!}
    </div>
    <!-- /.login-form -->
    {{ Html::image('img/bg-login.jpg','Glad Arabia Logo', ['class' => 'bg-login-img']) }}
</div>
<!-- /.login-bg -->
@endsection 
@section('pagescripts') {{ Html::script('vendor/bower_components/jquery-validation/dist/jquery.validate.min.js') }} @endsection
