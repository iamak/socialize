@extends('layout')
<!--  -->
@section('content')
<!-- Registration content starts -->
<div class="register-section clear">
    <a href="#" class="logo-holder">        
        {{ Html::image('img/glad-logo.png','Glad Arabia Logo') }}
    </a>
    <div class="glad-chef">
        {{ Html::image('img/glad-chef.png','Glad Chef') }}
    </div>
    <div class="register-form-container">
        <div class="register-header">
            <h2 class="italic">So you think you can cook?</h2> {{ Html::image('img/spatula.png', '', ['class'=>'spatula'])}}
            <h3>Share your Signature dish and make it to the <br>
                <span class="upper-case">2013 GLAD Cooking Challenge!</span>
            </h3>
        </div>
        <!-- /.register-header -->
       
        <div class="form-holder">
            {!! Form::open(['route' => 'register.post', 'files' => true, 'id'=>'registerForm']) !!}
            <div class="input-group">
                {{ Form::label('dish_photo', 'Upload photo of your dish', ['class' => 'required-field-label']) }}
                {{ Form::file('dish_photo', ['class' => 'hidden-file-input', 'accept' => 'image/*', 'required'=>'required']) }}
                <div class="file-input-holder clear">
                    {{ Form::label('dish_photo', 'choose file', ['class' => 'btn-browse']) }}
                    {{ Form::label('dish_photo', '(Format: JPG, Max size: 1MB )', ['class' => 'lbl-info']) }}
                </div>
                <!-- /.file-input-holder -->
            </div>
            <!-- /.input-group -->
            <div class="input-group">
                {{ Form::label('dish_name', 'Name of your dish', ['class' => 'required-field-label']) }}
                {{ Form::text('dish_name', '',['maxlength' => 200, 'required'=>'required']) }}
            </div>
            <!-- /.input-group -->
            <div class="input-group">
                {{ Form::label('dish_description', 'Why is this dish special to you?', ['class' => 'required-field-label']) }}
                {{ Form::textarea('dish_description','',['required'=>'required']) }}
            </div>
            <!-- /.input-group -->
            <div class="input-group">
                {{ Form::label('name', 'Name', ['class' => 'required-field-label']) }}
                {{ Form::text('name','',['maxlength' => 50, 'required'=>'required']) }}
            </div>
            <!-- /.input-group -->
            <div class="input-group">
                {{ Form::label('email', 'Email', ['class' => 'required-field-label']) }}
                {{ Form::email('email','',['maxlength' => 100, 'required'=>'required']) }}
            </div>
            <!-- /.input-group -->
            <div class="input-group">
                {{ Form::label('phoneno', 'Phone Number', ['class' => 'required-field-label']) }}
                {{ Form::tel('phoneno','',['maxlength' => 12, 'required'=>'required']) }}
            </div>
            <!-- /.input-group -->
            <div class="input-group">
                {{ Form::label('country', 'Country', ['class' => 'required-field-label']) }}
                <div class="custom-select">
                    {{ Form::select('country', ['' => 'Select Country'] + $Countries,'',['required'=>'required']) }}
                </div>
            </div>
            <!-- /.input-group -->
            <div class="input-group clear">
                <div class="age-holder">
                    {{ Form::label('age', 'Age', ['class' => 'required-field-label']) }}
                    {{ Form::tel('age','',['maxlength' => 2, 'required'=>'required']) }}
                </div>
                <!-- /.age-holder -->
                <div class="gender-holder">
                    <div class="radio-holder">
                        {{ Form::radio('gender', 'male', true, ['class' => 'custom-radio', 'id' => 'gender_male']) }}
                        <span class="custom-check"></span>
                        {{ Form::label('gender_male', 'Male', ['class' => 'radio-label']) }}
                    </div>
                    <div class="radio-holder pl-10">
                        {{ Form::radio('gender', 'female', false, ['class' => 'custom-radio', 'id' => 'gender_female']) }}
                        <span class="custom-check"></span>
                        {{ Form::label('gender_female', 'Female', ['class' => 'radio-label']) }}
                    </div>
                </div>
                <!-- /.gender-holder -->
            </div>
            <!-- /.input-group -->
            <div class="input-group clear">
                <div class="radio-holder agreement-holder">
                    <div class="left">
                        {{ Form::checkbox('agreement', 'agree', true, ['class' => 'custom-checkbox','id' => 'agreement', 'required' => 'required']) }}
                        <span class="custom-check"></span>
                    </div>
                    <label for="agreement" class="radio-label">
                        I agree to <a href="#" data-popup-link=".terms-popup">Terms and Conditions</a>
                    </label>
                </div>
             </div>
            <!-- /.input-group -->
            {{ Form::button ('Submit', ['type' => 'submit', 'class' => 'btn-register']) }}
            {!! Form::close() !!}
        </div>
        <!-- /.form-holder -->
    </div>
    <!-- /.register-form-container -->
</div>
<!-- /.register-section -->
@endsection

@section('pagestyles')
{{ Html::style('vendor/bower_components/sweetalert/dist/sweetalert.css') }}
@endsection
@section('pagescripts')
{{ Html::script('vendor/bower_components/jquery-validation/dist/jquery.validate.min.js') }}
{{ Html::script('vendor/bower_components/jquery-validation/dist/additional-methods.min.js') }}
{{ Html::script('vendor/bower_components/sweetalert/dist/sweetalert.min.js') }}
<script>
    $(document).ready(function() {
        $("#registerForm").validate({
            rules: {
                dish_photo: {
                    required: true,
                    accept: "image/*"
                },
                email: {
                   required: true,
                   email: true
                },
                name: {
                   required: true
                },
                age: {
                   required: true,
                   digits: true
                },
                phoneno: {
                   required: true,
                   digits: true
                }
            },
            messages: {
                dish_photo: {
                    required: "Select an Image file",
                    accept: "Please select a valid image file"
                }
            },
        });
    });
    @if(count($errors) > 0)
    $(window).on('load', function() {
        swal({
            title: "Error!",
            text: "@foreach ($errors->all() as $error){{ $error }}\n\r @endforeach",
            type: "error"
        });
    });
@endif
</script>

@endsection