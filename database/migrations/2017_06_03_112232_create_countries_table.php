<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('official_name')->nullable();
            $table->longtext('native_names')->nullable();
            $table->string('toplevel_domain')->nullable();
            $table->string('cca2')->nullable();
            $table->string('ccn3')->nullable();
            $table->string('cca3')->nullable();
            $table->string('cioc')->nullable();
            $table->string('currency')->nullable();
            $table->string('calling_code')->nullable();
            $table->string('capital')->nullable();
            $table->longtext('alt_spellings')->nullable();
            $table->longtext('languages')->nullable();
            $table->longtext('translations')->nullable();
            $table->string('region')->nullable();
            $table->string('subregion')->nullable();
            $table->string('latlng')->nullable();
            $table->string('demonym')->nullable();
            $table->string('borders')->nullable();
            $table->string('area')->nullable();

            $table->boolean('active')->default(1);
            $table->boolean('verified')->default(0);
            $table->boolean('published')->default(0);
            $table->boolean('blocked')->default(0);
            $table->boolean('deleted')->default(0);
            $table->string('created_by')->default('system');
            $table->string('created_ip')->default('localhost');
            $table->string('updated_by')->default('system');
            $table->string('updated_ip')->default('localhost');
            $table->string('deleted_by')->default('system');
            $table->string('deleted_ip')->default('localhost');
            $table->softDeletes();            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
