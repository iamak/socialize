<?php

use Illuminate\Database\Seeder;

use App\Country;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->truncate();

        $json = File::get('database/json/countries.json');

        $data = json_decode($json);

        foreach ($data as $country) {
            $countryData = [
                'name' => $country->name->common,
                'official_name' => $country->name->official,
                'native_names' => $country->name->native,
                'toplevel_domain' => $country->tld,
                'cca2' => $country->cca2,
                'ccn3' => $country->ccn3,
                'cca3' => $country->cca3,
                'cioc' => $country->cioc,
                'currency' => $country->currency,
                'calling_code' => $country->callingCode,
                'capital' => $country->capital,
                'alt_spellings' => $country->altSpellings,
                'languages' => $country->languages,
                'translations' => $country->translations,
                'region' => $country->region,
                'subregion' => $country->subregion,
                'latlng' => $country->latlng,
                'demonym' => $country->demonym,
                'borders' => $country->borders,
                'area' => $country->area,
                'active' => 1,
                'published' => 1,
            ];
            Country::create($countryData);
        }
    }
}
