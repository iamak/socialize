<?php

use Illuminate\Database\Seeder;

use App\Submission;

use Faker\Generator;

class SubmissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('submissions')->truncate();

        $faker = Faker\Factory::create();

        for ($i=1; $i <= 97; $i++) { 
        	
        	$data = [
				'name' => $faker->name,
				'email' => $faker->safeEmail,
				'phoneno' => $faker->e164PhoneNumber,
				'country' => $faker->country,
				'age' => '23',
				'gender' => 'male',
				'dish_name' => $faker->words(4,true),
				'dish_description' => $faker->sentence(10, true),
				'dish_photo' => '/uploads/images/'.$i.'.jpg',
				'verified' => 1,
				'published' => 1,
        	];

        	Submission::create($data);

        }
    }
}
