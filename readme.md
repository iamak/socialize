# Installation Requirements

1. You must have PHP and MySQL database installed in your system and should accesible globally / added tha paths to `$PATH` variable
2. You also need composer, node, node-cli, npm, bower, gulp, gulp-cli installed globally

# Installation Instructions

1. Unzip the folder to your local machine and open this folder in terminal
2. Run `composer update` from the terminal and it will update all dependencies
3. Also run 'npm install' to install all node dependencies if you need to use the gulp tool for compiling sass files
3. Create new database in your mysql localhost and Update `.env` file with your database information
4. Run `php artisan migrate --seed` if you are running it for the first time, to have the dummy data and the country lists.
5. Run `php artisan serve` to access the website on your localhost.