<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

Route::resource('/', 'SocialAppController',['names' => [
    'index' => 'login',
    'create' => 'register',
    'store' => 'register.post',
]]);
Route::get('/gallery', 'SocialAppController@gallery');
Route::get('/gallery/details/{submission}', 'SocialAppController@show');